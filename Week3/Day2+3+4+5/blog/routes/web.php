<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Facade\FlareClient\View;

Route::get('/template', function () {
    return view('welcome');
});

Route::get('/hello-laravel', function () {
    echo "halamab baru <br>";
    return "hello-laravel";
});

Route::get('/','HomeController@view');

// Route::get('/', function ()  {
//     return view('sanberbook');
// });


// Route::get('/register', function () {
//     return view('register');
// });
Route::get('/register', "AuthController@form")->name('register'); 
Route::post('/welcome', "AuthController@form_post");     

// Route::get('/welcome', "AuthController@next"); 

Route::get('/master',function(){
    return view('adminlte.master');
});

Route::get('/table',function(){
    return view('adminlte.items.table');
})->name('table');

Route::get('/data-tables',function(){
    return view('adminlte.items.data-table');
})->name('data-tables');

Route::get('/cast',"CastController@index");

Route::get('/cast/create',"CastController@create");

Route::post('/cast',"CastController@store");

Route::get('/cast/{id}',"CastController@show");

Route::get('/cast/{id}/edit',"CastController@edit");
Route::delete('/cast/{id}',"CastController@destroy");
