<!DOCTYPE html>
<html>
<body>
	<head>
	<title>Sign Up</title>
</head>

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<p>My first paragraph.</p>
<form action="/welcome" method="POST">
  @csrf
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname"><br>
  <input type="radio" id="male" name="gender" value="male">
  <label for="male">Male</label><br>
  <input type="radio" id="female" name="gender" value="female">
  <label for="female">Female</label><br>
  <input type="radio" id="other" name="gender" value="other">
  <label for="other">Other</label><br>
  <label>Nationality:</label><br>
  <select name="negara">
  	<option>Indonesian</option>
  	<option>Malaysian</option>
  	<option>Singaporean</option>
  	<option>Australian</option>
  </select><br>
  <input type="checkbox" id="indo" name="indo" value="indonesia">
  <label for="indo">Indonesia</label><br>
  <input type="checkbox" id="english" name="english" value="english">
  <label for="english">English</label><br>
  <input type="checkbox" id="other" name="other" value="other">
  <label for="other">Other</label>
  <label>Bio:</label><br>
  <textarea name="bio" cols="30" rows="10"></textarea><br><br>
  <input type="submit" name="submit">
</form>

</body>
</html>