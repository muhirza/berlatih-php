@extends('adminlte.master')
@section('content')
    <div class="mt-3 ml-3">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Casts</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">id</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse($cast as $key => $data)
                        <tr>
                            <td>{{$key +1 }}</td>
                            <td>{{$data->nama }}</td>
                            <td>{{$data->umur }}</td>
                            
                            <td>
                                
                                <a href='/cast/{{$data->id}}' class="btn btn-primary"> detil</a><br>
                                <a href='/cast/{{$data->id}}/edit' class="btn btn-primary mt-1"> edit</a>
                                <form action="/cast/{{$data->id}}" method='post'>
                                    @csrf
                                    @method("DELETE")
                                    <input type="submit" value="delete" class="btn btn-danger mt-1">
                                </form>
                               
                            </td>
                                
                            
                            <!-- <td><span class="badge bg-danger">55%</span></td> -->
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">No data</td>
                            </tr>  
                      @endforelse
                    <!-- <tr>
                      <td>1.</td>
                      <td>Update software</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-danger">55%</span></td>
                    </tr> -->
                    <!-- <tr>
                      <td>2.</td>
                      <td>Clean database</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar bg-warning" style="width: 70%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-warning">70%</span></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Cron job running</td>
                      <td>
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar bg-primary" style="width: 30%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-primary">30%</span></td>
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>Fix and squish bugs</td>
                      <td>
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar bg-success" style="width: 90%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-success">90%</span></td>
                    </tr> -->
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
    </div>
@endsection