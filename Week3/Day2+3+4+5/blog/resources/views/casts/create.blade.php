@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Cast Input</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name='nama' placeholder="Tulis Namamu">
                    
                      @error('nama')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name='umur'placeholder="Tulis umurmu">
                      @error('umur')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name='bio'placeholder="Tulis Biomu">
                      @error('bio')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                       <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div> -->
                    <!-- </div>
                  </div> --> 
                  <!-- <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div> -->
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
@endsection